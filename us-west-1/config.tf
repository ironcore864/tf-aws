# The default "aws" configuration is used for AWS resources in the root module where no explicit provider instance is selected.
provider "aws" {
  version = "~> 2.0"
  region  = "us-west-1"
}

# A non-default, or "aliased" configuration is also defined for a different region.
# Currently only used for docdb, which is not supported in us-west-1 yet.
provider "aws" {
  alias  = "usw2"
  region = "us-west-2"
}

terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket         = "s3-sei-tf-state"
    key            = "terraform.tfstate"
    region         = "us-west-1"
    encrypt        = true
    dynamodb_table = "ddb_sei_terraform_lock"
  }
}
