module "network" {
  source = "../modules/networking"
}

module "redis" {
  source = "../modules/redis"

  name       = "redis-cluster"
  subnet_ids = [module.network.private_subnet_1_id, module.network.private_subnet_2_id]
}

# module "docdb" {
#   source = "../modules/docdb"

#   providers = {
#     aws = aws.usw2
#   }

#   name       = "docdb"
#   subnet_ids = [module.network.private_subnet_1_id, module.network.private_subnet_2_id]
# }
