resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

resource "aws_eip" "nat_gateway_main" {
  vpc = true
}

resource "aws_nat_gateway" "main" {
  allocation_id = aws_eip.nat_gateway_main.id
  subnet_id     = aws_subnet.public[keys(var.public_subnet_azid_cidr_map)[0]].id
}
