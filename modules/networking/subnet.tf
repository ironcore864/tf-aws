resource "aws_subnet" "public" {
  for_each = var.public_subnet_azid_cidr_map

  vpc_id               = aws_vpc.main.id
  cidr_block           = each.value
  availability_zone_id = each.key
  tags = {
    Name = "Public-${each.key}"
  }
}

resource "aws_subnet" "private" {
  for_each = var.private_subnet_azid_cidr_map

  vpc_id               = aws_vpc.main.id
  cidr_block           = each.value
  availability_zone_id = each.key
  tags = {
    Name = "Private-${each.key}"
  }
}
