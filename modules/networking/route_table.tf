resource "aws_route_table" "public" {
  vpc_id = aws_vpc.main.id

  # Note that the default route, mapping the VPC's CIDR block to "local", is created implicitly and cannot be specified.
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = {
    Name = "public"
  }
}

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main.id
  }

  tags = {
    Name = "private"
  }
}

resource "aws_route_table_association" "public_subnet" {
  for_each = var.public_subnet_azid_cidr_map

  subnet_id      = aws_subnet.public[each.key].id
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "private_subnet" {
  for_each = var.private_subnet_azid_cidr_map

  subnet_id      = aws_subnet.private[each.key].id
  route_table_id = aws_route_table.private.id
}
