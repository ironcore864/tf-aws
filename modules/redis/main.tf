resource "aws_elasticache_subnet_group" "redis" {
  name       = var.name
  subnet_ids = var.subnet_ids
}

resource "aws_elasticache_replication_group" "redis" {
  replication_group_id          = var.name
  replication_group_description = var.name
  subnet_group_name             = aws_elasticache_subnet_group.redis.name
  node_type                     = var.node_type
  port                          = 6379
  automatic_failover_enabled    = true

  cluster_mode {
    replicas_per_node_group = 1
    num_node_groups         = 1
  }
}
