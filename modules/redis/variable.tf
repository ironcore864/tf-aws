variable "subnet_ids" {
  type = list(string)
}

variable "name" {
  type = string
}

variable "node_type" {
  type    = string
  default = "cache.t3.micro"
}
