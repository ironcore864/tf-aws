variable "name" {
  type = string
}

variable "master_username" {
  type    = string
  default = "masteruser"
}

variable "subnet_ids" {
  type = list(string)
}

variable "instance_number" {
  type    = number
  default = 3
}
