# not used yet

resource "random_password" "password" {
  length           = 8
  special          = true
  override_special = "_%@"
}

resource "aws_db_subnet_group" "default" {
  name       = var.name
  subnet_ids = var.subnet_ids
}

resource "aws_docdb_cluster" "docdb" {
  cluster_identifier   = var.name
  engine               = "docdb"
  db_subnet_group_name = aws_db_subnet_group.default.name
  master_username      = var.master_username
  master_password      = random_password.password.result
  skip_final_snapshot  = true
}

resource "aws_docdb_cluster_instance" "cluster_instances" {
  count              = var.instance_number
  identifier         = "docdb-cluster-demo-${count.index}"
  cluster_identifier = aws_docdb_cluster.docdb.id
  instance_class     = "db.r5.large"
}
