output "master_username" {
  value = var.master_username
}

output "master_password" {
  value = random_password.password.result
}
