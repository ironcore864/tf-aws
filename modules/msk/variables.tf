variable "subnet_ids" {
  type = list(string)
}

variable "vpc_id" {
  type = string
}

variable "cluster_name" {
  type = string
}

variable "node_type" {
  type    = string
  default = "cache.t3.micro"
}
