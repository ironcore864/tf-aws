# not finished, not used yet

resource "aws_security_group" "kafka" {
  vpc_id = var.vpc_id
}

resource "aws_msk_cluster" "example" {
  cluster_name = var.cluster_name
  # AWS recommended version
  kafka_version          = "2.2.1"
  number_of_broker_nodes = 2

  broker_node_group_info {
    instance_type   = "kafka.m5.large"
    ebs_volume_size = 1000
    client_subnets  = var.subnet_ids
    security_groups = [aws_security_group.kafka.id]
  }

  open_monitoring {
    prometheus {
      jmx_exporter {
        enabled_in_broker = true
      }
      node_exporter {
        enabled_in_broker = true
      }
    }
  }
}
