module "network" {
  source = "../modules/networking"

  vpc_cidr_block               = var.vpc_cidr_block
  public_subnet_azid_cidr_map  = var.public_subnet_azid_cidr_map
  private_subnet_azid_cidr_map = var.private_subnet_azid_cidr_map
}
