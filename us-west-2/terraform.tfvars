vpc_cidr_block = "10.1.0.0/16"
public_subnet_azid_cidr_map = {
  "usw2-az1" = "10.1.1.0/24"
  "usw2-az2" = "10.1.2.0/24"
}
private_subnet_azid_cidr_map = {
  "usw2-az1" = "10.1.3.0/24"
  "usw2-az2" = "10.1.4.0/24"
}
