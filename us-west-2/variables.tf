variable "vpc_cidr_block" {
  type = string
}

variable "public_subnet_azid_cidr_map" {
  type = map
}

variable "private_subnet_azid_cidr_map" {
  type = map
}
