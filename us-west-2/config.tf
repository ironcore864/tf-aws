# The default "aws" configuration is used for AWS resources in the root module where no explicit provider instance is selected.
provider "aws" {
  version = "~> 2.0"
  region  = "us-west-2"
}

terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket         = "s3-sei-tf-state-us-west-2"
    key            = "skyway"
    region         = "us-west-2"
    encrypt        = true
    dynamodb_table = "ddb_sei_terraform_lock"
  }
}
