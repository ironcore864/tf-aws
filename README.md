# AWS Infrastructure Initialization with Terraform

## Architecture

![](doc/images/sei-aws.png)

## Dependencies

- terraform >= 0.12.23

## Manual Steps

Do this only for a completely new account:

- Create s3 bucket: `s3-sei-tf-state` (must be a different name for each account, used in [config.tf](config.tf))
- Create DynamoDB: `ddb_sei_terraform_lock` with primary key named `LockID` (used in [config.tf](config.tf))

## Deploy

You need to have aws region/access key id/secret configured first, located in `~/.aws/config` and `~/.aws/credentials`.

Then run:

```
terraform init
terraform plan -out tfplan
terraform apply tfplan
```

## EC2 Access

If we deploy EC2 in the private subnet which only has a NAT gateway, we must use Bastion to access them. See:

https://aws.amazon.com/quickstart/architecture/linux-bastion/
https://docs.aws.amazon.com/quickstart/latest/linux-bastion/welcome.html

The deployment of bastion is using cloudformation stack.
